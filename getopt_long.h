/*
 * sntop - simple network top - a top-like console network status tool
 * getopt.h - declarations needed for getopt_long()
 * 07.31.2000
 *
 * robert m love	<rml@tech9.net>
 * chris m rivera	<cmrivera@ufl.edu>
 *
 * This is free software under the GNU Public License (see COPYING)
 *
 * Copyright (C) 2000 Robert M. Love and Christopher M. Rivera
 *
 * All code by the above authors unless otherwise specified.
 */

#ifndef _GETOPT_LONG_H
#define _GETOPT_LONG_H 1

extern char *optarg;
extern int optind;
extern int opterr;
extern int optopt;

struct option {
  const char *name;
  int has_arg;
  int *flag;
  int val;
};

extern int getopt_long (int argc, char *const *argv, const char *shortopts,
		        const struct option *longopts, int *longind);

#endif /* _GETOPT_LONG_H */
