/*
 * sntop - simple network top - a top-like console network status tool
 * sntop.h
 *
 * robert m love	<rml@tech9.net>
 * chris m rivera	<cmrivera@ufl.edu>
 *
 * http://sntop.sourceforge.net - homepage
 * ftp://sntop.sourceforge.net/pub/sntop/ - anon ftp
 *
 * Licensed under the GNU General Public License v2
 * 
 * Copyright (C) 2001 Robert M. Love and Christopher M. Rivera
 */

#define SNTOP_AUTHORS	"Robert M. Love <rml@tech9.net>, Chris M. Rivera <cmrivera@ufl.edu>"
#define IN_BUF		33 /* size of our conf data vars and their input */
#define CONF_BUF	256 /* size of path to conf data */
#define CONF_UFILE	"/.sntoprc" /* name of user conf file .. we look for this in ~ first */
#ifndef CONF_SFILE
#define CONF_SFILE	"/etc/sntoprc"
#endif
#define HTML_FILE	"sntop.html" /* location and name of optional html output file */
#define REFRESH_TIME	180 /* time to sleep in seconds between refreshes */
#define FPING_LINE	"fping -r 1 -b 24 -q" /* send upto (2) 24-byte pings, in quiet mode */
#define PING_LINE	"ping -c 1 -s 8 -q" /* send (1) 8-byte ping, in quiet mode */
#define PING_END	" 2> /dev/null > /dev/null"
#define PING_BUF	20 /* string size of the largest nPING_LINE */
#define PEND_BUF	25 /* string size of PING_END */
#define ACTION_BUF	64 /* size of do-on-alarm action string */
#define UP		"UP"
#define DOWN		"DOWN"
#define HTML_UP		"\"#00BB00\">"UP
#define HTML_DOWN 	"\"#CC0000\">"DOWN
#define DO_HTML		1
#define SECURE_MODE	2
#define DO_ONCE		4
#define DO_ALARM	8
#define DO_LOG		16
#define DO_COLOR	32
#define DAEMON          64

typedef struct hdc_ {
     char name[IN_BUF];
     char host[IN_BUF];
     char note[IN_BUF];
     char *status;
     struct hdc_ *next;
} hdc;

char * get(char *, int, char, FILE *); /* a smarter, better fgets() */
void abortion(const char *, int); /* cleanup and exit on error */
RETSIGTYPE aborted(int); /* SIGINT hook */
unsigned short int sntop_exec(char *); /* a smarter, better system() */
void load_config(char *, hdc **);
void exec_alarm(char *, char *, char *, char *);
void start_html(char *, FILE **, unsigned int);
void add_html(FILE *, char *, char *, char *);
void end_html(FILE *, unsigned int, unsigned int);

/* EOF */
