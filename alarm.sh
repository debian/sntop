#! /bin/sh
# alarm.sh - Sample script for use with sntop's alarm/log mode
# rml, 20001022

# sntop supports two-modes, alarm and log, set via:
#  -a <file>, --alarm=file
#  -l <file>, --log=file
# In alarm mode, whenever a site goes DOWN <file> is executed
# In log mode, <file> is executed on any change in status
# <file> is given three arguments: the display hostname, the
#  actual hostname or IP address, and the new status (UP or DOWN)

# This script will log to OUTFILE the time and date, the display
#  name, and the status of the passed host.  If the host is DOWN
#  (always the case in alarm mode), an e-mail is sent to root

OUTFILE=sntop.log

if [ $3 = "DOWN" ]; then
	echo "sntop detected at `date` that $1 [$2] went $3" | mail -s "[sntop] Host Down!" root
fi

echo "sntop: `date`: $1 went $3" >> ${OUTFILE}

# EOF
